# Metagenomics

Utilities to download/clean genbank for data analysis


## Dependencies and install 

requirements:
* Python 3.X
* BioPython
* taxonkit


Following section describes the install process on the genologin server.

### install taxonKit (extract taxonomy with required levels, and fill missing levels)

website: [taxonKit](https://bioinf.shenwei.me/taxonkit/)

download the appropriate version: [download](https://github.com/shenwei356/taxonkit/releases/tag/v0.8.0)

```
cd $HOME
wget https://github.com/shenwei356/taxonkit/releases/download/v0.8.0/taxonkit_linux_amd64.tar.gz
tar -zxvf taxonkit_linux_amd64.tar.gz
```
get NCBI taxonomy DB:
```
wget -c ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz 
tar -zxvf taxdump.tar.gz

mkdir -p $HOME/.taxonkit
mv *.dmp $HOME/.taxonkit
```

### install metagenomics code

download the code: [here](https://forgemia.inra.fr/bioger/metagenomics) 

```
wget https://forgemia.inra.fr/bioger/metagenomics/-/archive/main/metagenomics-main.tar.gz
tar -xzf metagenomics-main.tar.gz
```
add the code to your .bashrc
```
export PATH="$HOME/metagenomics-main:$PATH"
```

### run

to launch the different tools, load the Python module:

```
module load system/Python-3.7.4
```

## Download DB from genbank

### 1) download actin or its with predefined query:
```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr actin.gb -v 2 --actin
```
or
```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr its.gb -v 2 --its
```

### 2) download with user-defined query:

```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr out.gb -v 2 --query "fungi[Title] OR turtulle[Title] AND (100 [SLEN] : 200 [SLEN])"

```

### 3) Check the number of entries (without download):

```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr out.gb -v 2 --check_query --query "fungi[Title] OR turtulle[Title] AND (100 [SLEN] : 200 [SLEN])"
```

### 4) Restart a download from a IDs list with start point

```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr out.gb -v 2 --IDs myIDs.txt --start 34000 
```

### 5) Update an available genbank file (keep required accessions and download new ones)
```
DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr out.gb -v 2 --update previous.gb 
```

Temporary data are stored in `$PWD\tmp`. Remove the folder at the end.

### Note: ITS db

The ITS dataset contains around 150000 sequence. For a such a query, the download process is long. To launch the process in background write a simple script to launch on the cluster:

*cluster.sh*
```shell
#!/bin/sh

#SBATCH -J download
#SBATCH -o download.stdout
#SBATCH -e download.stderr
#SBATCH --mem=10G
#SBATCH -c 1
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --export=ALL

module load system/Python-3.7.4

DownloadEntriesFromGenbank.py nicolas.lapalu@inrae.fr its.gb -v 2 --its

```
then launch:
```
sbatch cluster.sh
```


## Build database for Frogs (and MetagPipe)

Note: On Genologin, launch Python module:
```
module load system/Python-3.7.4
```

### 1) extract fasta sequences as previously
```
GetFastaFileFromGenBankFile.py -i actin.gb -o gb.fasta
```

### 2) extract taxon id of all sequences:
```
grep -P "/db_xref=\"taxon" actin.gb | cut -f 2 -d: | sed 's/\"//' > taxids.txt
```
check the number with:
```
wc -l taxids.txt
```

### 3) get 7 levels of taxonomy

Get lineage from taxid with only necessary taxonomic levels: Kingdom, phylum, class, order, family, genus, species.
Problem, taxonKit does not extract the kingdom, but the super-kingdom. So, we have to replace "Eukaryota" with "Fungi".
```
$HOME/taxonkit reformat taxids.txt -I 1 -F -f "{k};{p};{c};{o};{f};{g};{s}" | sed 's/Eukaryota/Fungi/' > taxons.list
```

### 4) Get your DB as necessary 3 available outputs

*output 1: default output, write the fasta / mapping file*:
```
WriteTaxonomyMappingAndFastaFiles.py gb.fasta actin.gb

```

*output 2 : write the fasta / mapping file with the taxons in the taxons.list, only 7 levels*:
```
WriteTaxonomyMappingAndFastaFiles.py gb.fasta actin.gb -t taxons.list
``` 

*output 3 : write the fasta / mapping file with the taxons in the taxons.list, only 7 levels formatted for Frogs*:
```
WriteTaxonomyMappingAndFastaFiles.py gb.fasta actin.gb -t taxons.list --frogs
``` 

if you want to have the accesion ID at the end of the taxonomy, add `--acc` to your command.


## misc

Queries (Angelique)

*ITS*:
```
fungi[All Fields] AND (internal transcribed spacer[Title] OR its1[Title] OR its2[Title]) AND (CBS*[Title] OR NRRL*[Title] OR MUCL*[Title] OR FRC*[Title] OR CPC*[Title] OR voucher*[Title] OR DAOM*[Title] OR MAFF*[Title] OR MIAE*[Title] OR IPO*[Title] OR ATCC*[Title] OR LMSA*[Title] OR IMI*[Title] OR CNRMA*[Title] OR NBRC*[Title] OR IBCN*[Title] OR DTO*[Title] OR BRIP*[Title] OR AFTOL*[Title] OR ICMP*[Title] OR MRC*[Title]) AND (100[SLEN] : 3000[SLEN]) NOT Ascomycete[Title] NOT Fungal endophyte[Title] NOT Uncultured[Title] NOT Vouchered mycorrhizae[Title] NOT Viridiplantae[All Fields] NOT sp.[Title] NOT IonTorrent[All Fields]
```

*Actin*:
```
fungi[All Fields] AND (actin[Title] OR act1[Title] OR actA[Title]) AND (CBS*[Title] OR NRRL*[Title] OR MUCL*[Title] OR FRC*[Title] OR CPC*[Title] OR voucher*[Title] OR DAOM*[Title] OR MAFF*[Title] OR MIAE[Title] OR IPO[Title] OR ATCC*[Title] OR LMSA*[Title]] OR IMI*[Title] OR CNRMA*[Title] OR NBRC*[Title] OR IBCN*[Title] OR DTO*[Title] OR BRIP*[Title] OR AFTOL*[Title] OR ICMP*[Title] OR MRC*[Title]) AND (100 [SLEN] : 3000 [SLEN]) NOT Ascomycete[Title] NOT Fungal endophyte[Title] NOT Uncultured[Title] NOT Vouchered mycorrhizae[Title] NOT Viridiplantae[All Fields] NOT sp.[Title] NOT IonTorrent[All Fields]
`
