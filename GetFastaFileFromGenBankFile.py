#! /usr/bin/env python3

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

import getopt
import sys
import re

class GetFastaFileFromGenBank():

	verbose = False

	def __init__(self):
		self._gbFile = None
		self._fastaFile = "output.fasta"
		self._excludeEnvironmentalSamples = False

	def setAttributesFromCmdLine(self):
		try:
			opts, args = getopt.getopt(sys.argv[1:],"fvi:o:",["gbfile=","fastafile=","filter","verbose","help"])
		except getopt.GetoptError as err:
			print(str(err))
			self.help()
		for o,a in opts:
			if o in ("-v","--verbose"):
				verbose = True
			elif o in ("-f","--filter"):
				self._excludeEnvironmentalSamples = True
			elif o in ("--help","--usage"):
				self.help()
				sys.exit()
			elif o in ("-i","--gbfile"):
				self._gbFile = a
			elif o in ("-o","--fastafile"):
				self._fastaFile = a
			else:
				assert False, "unhandled option"
				self.help()

		if not self._gbFile:
			print("\n ERROR: missing parameter -i or --gbFile")
			self.help()
		if not self._fastaFile:
			print("\n No file for output, default file \"output.fasta\" used")

	def help(self):
		print("\n")
		print("usage: %s [ options ]" % (sys.argv[0]))
		print("\n")
		print("args (mandatory):")
		print("  -i or --gbfile: genbank file to read")
		print("\n")
		print("args (optional):")
		print("  -o or --fastafile: fasta file to write, default output.fasta")
		print("  -f or --filter: exclude environmental samples and mis-identified sequences")
		print("  -v or --verbose")
		print("  --help, --usage")

		sys.exit()


	def exe(self):

		input_handle = open(self._gbFile, "r")
		output_handle = open(self._fastaFile, "w")

		sequences = []
		count = 0

		for seq_record in SeqIO.parse(input_handle, "genbank"):

			rec = seq_record
			rec.description = "(" + seq_record.annotations['organism'] + ")"
			if self._excludeEnvironmentalSamples:
				list = []
				list.extend(seq_record.annotations['taxonomy'])
				list.append(seq_record.annotations['source'])
				list.append(seq_record.annotations['organism'])
				m = re.search(r"soilfungal|rootassociatedfungal|sample|environmental|fungal.*sp|Other|endophyte| sp.| cf.| f.|fungal|leaf|litter|ascomycet|strain|aff.",";".join(list))
				if not m :
					SeqIO.write(rec, output_handle, "fasta")
					count += 1
			else:
				SeqIO.write(rec, output_handle, "fasta")
				count += 1

		output_handle.close()
		input_handle.close()
		print("%i entries converted to fasta" % count)

if __name__=="__main__":
	iGetFastaFileFromGenBank = GetFastaFileFromGenBank()
	iGetFastaFileFromGenBank.setAttributesFromCmdLine()
	iGetFastaFileFromGenBank.exe()

