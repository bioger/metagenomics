#! /usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from Bio import SeqIO
from Bio import GenBank
import re
import getopt
import sys
import os
import argparse
#from commons.core.utils.RepetOptionParser import RepetOptionParser
#from commons.core.utils.FileUtils import FileUtils

class WriteTaxonomyMappingAndFastaFiles(object):

    def __init__(self):
        self._fastaFileName = ""
        self._genbankFileName = ""
        self._taxonFileName = ""
        self._seqList = []
        self._id = 0


    def setAttributesFromCmdLine(self):
        description = "WriteTaxonomyMappingAndFastaFiles writes taxonomy mapping file and new reference fasta file from reference genbank and fasta files.\n" 
        usage = "WriteTaxonomyMappingAndFastaFiles.py <fasta file> <genbank file> -t <taxon_mapping>"
        parser = argparse.ArgumentParser(description = description, usage = usage)
        parser.add_argument("fasta",  type=str, help="input file (fasta type)")
        parser.add_argument("genbank", type=str, help="input file (genbank type)")
        parser.add_argument("-t","--taxon", default="", type=str, help="input file (taxon type) obtained with TaxonKit")
        parser.add_argument("--frogs", default=False, action="store_true", help="Export fasta file only with 7 levels required for FROGS")
        parser.add_argument("--acc", default=False, action="store_true", help="Export fasta file with accession id at the end of species name")
        args = parser.parse_args()

        self._fastaFileName = args.fasta
        self._genbankFileName = args.genbank
        self._taxonFileName = args.taxon
        self.frogs = args.frogs
        self.acc = args.acc


    def checkOptions(self):

        if self.frogs and self._taxonFileName == "":
            raise Exception("ERROR: Export in frogs format required a taxon file with -t option")

        if self._fastaFileName != "":
            if not os.path.isfile(self._fastaFileName):
                raise Exception("ERROR: fasta file %s does not exist!" % self._fastaFileName)
        if self._genbankFileName != "":
            if not os.path.isfile(self._genbankFileName):
                raise Exception("ERROR: genbank file %s does not exist!" % self._genbankFileName)
        if self._taxonFileName != "":
            if not os.path.isfile(self._taxonFileName):
                raise Exception("ERROR: taxon file %s does not exist!" % self._taxonFileName)

    def run(self):
        self.checkOptions()
        dIdToTaxonomy = {}
        fastaPath, ext = os.path.splitext(self._fastaFileName)

        if not self._taxonFileName:

            self._taxonMappingFile = open("%s_taxonMappingID.txt" % fastaPath, "w")
            self._newFastaFile = open("%s_taxonMappingID.fasta" % fastaPath, "w")

            for seq_genbank_record in SeqIO.parse(self._genbankFileName, "genbank"):
                tax = seq_genbank_record.annotations["taxonomy"]
                tax.append(seq_genbank_record.annotations["source"])
                dIdToTaxonomy[seq_genbank_record.id] = tax

            for seq_record in SeqIO.parse(self._fastaFileName, "fasta"):
                if  seq_record.id in dIdToTaxonomy:
                    taxonomy = ";".join(dIdToTaxonomy[seq_record.id])
                    self._writeTaxonomyMappingIdFile(taxonomy, seq_record.id)
                    self._writeNewFastaFile(seq_record)

            self._taxonMappingFile.close()
            self._newFastaFile.close()

        else:
            taxons = {}
            with open(self._taxonFileName, 'r') as f:
                for line in f:
                    val = line.rstrip().split("\t")
                    if len(val) == 2:
                        taxons[val[0]] = val[1]
                    else:
                        taxons[val[0]] = "unknown;unknown;unknown;unknown;unknown;unknown;unknown"

            f.close()

            with open(self._genbankFileName, 'r') as f:
                for seq_genbank_record in GenBank.parse(f):
                    taxid = None
                    for feat in seq_genbank_record.features:
                        if feat.key == 'source':
                            for qual in feat.qualifiers:
                                if qual.key == "/db_xref=":
                                    m = re.search(r"taxon:(\d+)", qual.value)
                                    if m:
                                        taxid =  m.group(1)
                                        if taxid in taxons:
                                            dIdToTaxonomy[seq_genbank_record.version] = taxons[taxid]
                                        else:
                                            print("missing taxon {}, go to next seq".format(taxid))
                    if not taxid:
                        raise Exception("Missing taxid in source for feature {}".format(seq_genbank_record.version))
            f.close()

            if self.frogs:

                self._newFastaFile = open("%s_taxonMappingID.fasta" % fastaPath, "w")

                for seq_record in SeqIO.parse(self._fastaFileName, "fasta"):
                    if seq_record.id in dIdToTaxonomy:
                        self._writeNewFastaFileFROGS(seq_record, dIdToTaxonomy[seq_record.id], seq_record.id)
                    else:
                        #raise Exception("Problem with taxonomy for accession {}".format(seq_record.id))
                        print("Problem with taxonomy for accession {}".format(seq_record.id))

                self._newFastaFile.close()

            else:

                self._taxonMappingFile = open("%s_taxonMappingID.txt" % fastaPath, "w")
                self._newFastaFile = open("%s_taxonMappingID.fasta" % fastaPath, "w")

                for seq_record in SeqIO.parse(self._fastaFileName, "fasta"):
                    if  seq_record.id in dIdToTaxonomy:
                        self._writeTaxonomyMappingIdFile(dIdToTaxonomy[seq_record.id],seq_record.id)
                        self._writeNewFastaFile(seq_record)
                    else:
                        raise Exception("Problem with taxonomy for accession {}".format(seq_record.id))

                self._taxonMappingFile.close()
                self._newFastaFile.close()

    def _clean_char(self, taxo):

        s = re.sub(r'[^0-9a-zA-Z;_]+', '_', taxo)
        s = re.sub(r';_+', ';', s)
        s = re.sub(r'_+;', ';', s)
        return s

    def _writeTaxonomyMappingIdFile(self, taxonomy, accession):
        self._id += 1
        if self.acc:
            self._taxonMappingFile.write("%s\t%s_%s;\n" % (self._id, self._clean_char(taxonomy), accession))
        else:
            self._taxonMappingFile.write("%s\t%s;\n" % (self._id, self._clean_char(taxonomy)))

    def _writeNewFastaFile(self, iSeq):
        self._newFastaFile.write(">%s %s\n" % (self._id,iSeq.id))
        self._newFastaFile.write("%s\n" % iSeq.seq)

    def _writeNewFastaFileFROGS(self, iSeq, taxo, accession):
        if self.acc:
            self._newFastaFile.write(">%s\t%s_%s\n" % (iSeq.id, self._clean_char(taxo), accession))
        else:
            self._newFastaFile.write(">%s\t%s\n" % (iSeq.id, self._clean_char(taxo)))
        self._newFastaFile.write("%s\n" % iSeq.seq)


if __name__=="__main__":
    iWTMAFF = WriteTaxonomyMappingAndFastaFiles()
    iWTMAFF.setAttributesFromCmdLine()
    iWTMAFF.run()
