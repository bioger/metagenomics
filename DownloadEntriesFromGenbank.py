#! /usr/bin/env python3

import argparse
import sys
import time
import os
import glob
import logging
import multiprocessing
import shutil
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

from Bio import SeqIO
from Bio import Entrez
from Bio import GenBank

class DownloadEntriesFromGenbank():

    def __init__(self, email, output, actin, its, ef1, query, check_query, IDs, start, update):

        self._email = email
        self.output = output
        self.query = query
        self.check_query = check_query
        self._batch_size = 1000
        self.IDs = IDs
        self.start = start
        self.update = update
        self.status = 0

        if actin:
            self.query = "fungi[All Fields] AND (actin[Title] OR act1[Title] OR actA[Title]) AND (CBS*[Title] OR NRRL*[Title] OR MUCL*[Title] OR FRC*[Title] OR CPC*[Title] OR voucher*[Title] OR DAOM*[Title] OR MAFF*[Title] OR MIAE[Title] OR IPO[Title] OR ATCC*[Title] OR LMSA*[Title]] OR IMI*[Title] OR CNRMA*[Title] OR NBRC*[Title] OR IBCN*[Title] OR DTO*[Title] OR BRIP*[Title] OR AFTOL*[Title] OR ICMP*[Title] OR MRC*[Title]) AND (100 [SLEN] : 3000 [SLEN]) NOT Ascomycete[Title] NOT Fungal endophyte[Title] NOT Uncultured[Title] NOT Vouchered mycorrhizae[Title] NOT Viridiplantae[All Fields] NOT sp.[Title] NOT IonTorrent[All Fields]"
        elif its:
            self.query = "fungi[All Fields] AND (internal transcribed spacer[Title] OR its1[Title] OR its2[Title]) AND (CBS*[Title] OR NRRL*[Title] OR MUCL*[Title] OR FRC*[Title] OR CPC*[Title] OR voucher*[Title] OR DAOM*[Title] OR MAFF*[Title] OR MIAE*[Title] OR IPO*[Title] OR ATCC*[Title] OR LMSA*[Title] OR IMI*[Title] OR CNRMA*[Title] OR NBRC*[Title] OR IBCN*[Title] OR DTO*[Title] OR BRIP*[Title] OR AFTOL*[Title] OR ICMP*[Title] OR MRC*[Title]) AND (100[SLEN] : 3000[SLEN]) NOT Ascomycete[Title] NOT Fungal endophyte[Title] NOT Uncultured[Title] NOT Vouchered mycorrhizae[Title] NOT Viridiplantae[All Fields] NOT sp.[Title] NOT IonTorrent[All Fields]"
        elif ef1:
            self.query = "fungi[All Fields] AND (translation elongation factor[Title] OR tef1[Title] OR translation elongation factor-1 alpha[Title] OR elongation[Title] OR EF1-a[Title] OR tef-1[Title]) AND (CBS*[Title] OR NRRL*[Title] OR MUCL*[Title] OR FRC*[Title] OR CPC*[Title] OR voucher*[Title] OR DAOM*[Title] OR MAFF*[Title] OR MIAE*[Title] OR IPO*[Title] OR ATCC*[Title] OR LMSA*[Title]] OR IMI*[Title] OR CNRMA*[Title] OR NBRC*[Title] OR IBCN*[Title] OR DTO*[Title] OR BRIP*[Title] OR AFTOL*[Title] OR ICMP*[Title] OR MRC*[Title]) AND (100 [SLEN] : 3000 [SLEN]) NOT Ascomycete[Title] NOT Fungal endophyte[Title] NOT Uncultured[Title] NOT Vouchered mycorrhizae[Title] NOT Viridiplantae[All Fields] NOT sp.[Title] NOT IonTorrent[All Fields] NOT (TEF3 [Title] OR translation elongation factor 3 [Title] OR tef-3 [Title] OR elongation factor 3[Title] OR EF-3[Title])"
        elif query:
            self.query = query
        elif self.start and self.IDs:
           logging.info("No query, restart download from {} with {}".format(self.start, self.IDs))

        else:
            logging.info("No query to perform, please choose one option")
            raise Exception

        if self.query:
            logging.info("Performing entry selection based on:\n{}".format(self.query))


    def getIDList(self):

        nbEntries = 0
        # get the number of entries
        logging.info("Requesting number of entries")
        handle = Entrez.egquery(term=self.query)
        record = Entrez.read(handle)
        for row in record["eGQueryResult"]:
            if row["DbName"]=="nuccore":
                nbEntries = row["Count"]

        logging.info(" %s entries found for request" %(nbEntries))

        if self.check_query:
            sys.exit(0)

        # get ID list
        logging.info("Get list of IDs")
        lIds = []
        batch=100
        #handle = Entrez.esearch(db="nuccore",term=self.query, restart=0, retmax=batch, usehistory="y",idtype="acc")
        handle = Entrez.esearch(db="nuccore",term=self.query, retmax=nbEntries, idtype="acc")
        
        #handle = Entrez.esearch(db="nuccore",term=self.query, retmax=nbEntries, usehistory="y",idtype="acc")
        #handle = Entrez.esearch(db="nuccore",term=self.query)
        record = Entrez.read(handle)

#        id_list = record['IdList']
        lIds = record['IdList']
#        print(id_list)
#        print(len(id_list))
#        post_xml = Entrez.epost("pubmed", id=",".join(id_list))
#        search_results = Entrez.read(post_xml)
##        webenv = record["WebEnv"]
##        querykey = record["QueryKey"]
##        lIds.extend(record["IdList"])
#        webenv = search_results["WebEnv"]
#        querykey = search_results["QueryKey"]
##        lIds.extend(record["IdList"])
#        print(nbEntries)
#        #for i in range(batch, int(nbEntries), batch):
#
#
#        try:
#            from urllib.error import HTTPError  # for Python 3
#        except ImportError:
#            from urllib2 import HTTPError  # for Python 2
#
#        for i in range(0, int(nbEntries), batch):
#            #handle = Entrez.esearch(db="nuccore",term=self.query,webenv=webenv, query_key=querykey, restart=i, retmax=batch, usehistory="y",idtype="acc")
#            handle = Entrez.efetch(db="nuccore",webenv=webenv, query_key=querykey, restart=i, retmax=batch)
#            record = handle.read(handle)
#            print(len(lIds),i)
#            print(record["IdList"][0])
#            lIds.extend(record["IdList"])
#            handle.close()
#

        return lIds

    def getIDListFromFile(self):

        lIds = []
        with open(self.IDs, 'r') as f:
            for line in f:
                lIds.append(line.rstrip())
        logging.info("{} ID entries found in {}".format(len(lIds), self.IDs))

        return lIds

    def saveIDList(self, lIds):

        with open("lIDs.list.txt", 'w') as f:
            for l in lIds:
                f.write("{}\n".format(l))
        logging.info("ID list saved in lIds.list.txt")


    def downloadEntries(self, start, end, IDs):

        filename = "tmp/{}_{}_tmp.gb".format(start,end)

        logging.info("Going to download record %i to %i" % (start, end))

        if os.path.isfile(filename):
            print("%s exists, will be removed and replaced" % (filename))

#        # post Ids list at NCBI
#        search_results = Entrez.read(Entrez.epost("nuccore", id=",".join(IDs)))
#        webenv = search_results["WebEnv"]
#        query_key = search_results["QueryKey"]
#
#        logging.debug("webenv : %s " % (webenv))
#        logging.debug("query_key : %s " % (query_key))
#
        success = False
        tries = 0
        while success is False:
            try:

                # post Ids list at NCBI
                search_results = Entrez.read(Entrez.epost("nuccore", id=",".join(IDs)))
                webenv = search_results["WebEnv"]
                query_key = search_results["QueryKey"]
        
                logging.debug("webenv : %s " % (webenv))
                logging.debug("query_key : %s " % (query_key))

                # Downloading...
                net_handle = Entrez.efetch(db="nucleotide",rettype="gb",webenv=webenv, query_key=query_key)

                out_handle = open(filename, "w")
                out_handle.write(net_handle.read())

                out_handle.close()
                net_handle.close()
                success = True
            except Exception as err:
                tries = tries + 1
                logging.error("Error in download from %s to %s" % (start, end))
                if tries > 4:
                    logging.error("Failed to download after 4 tries, continue with next batch")
                    self.status = 99
                    break

        try:
            self.getNumberOfRecords(filename, len(IDs))
        except:
            logging.info("exception occured ... nevertheless ... continue ")
           # raise Exception

        logging.info("record %i to %i - Done" % (start, end))

        return filename

    def getNumberOfRecords(self, filename, nbEntries):

        try:
            records = list(SeqIO.parse(filename, "genbank"))
            if len(records) < nbEntries:
                logging.info("{} entries found instead of {} for genbank file {}".format(len(records), nbEntries, filename))
                raise Exception
        except:
            logging.info("cannot read filename: {} not genbank file or missing entries".format(filename))
            raise Exception


    def convertIDList(self, lIDs):

        return [str(i) for i in lIDs]

    def getIDListToUpdate(self, lIds):

        lKnownIDs = []
        with open(self.update) as handle:
            for record in GenBank.parse(handle):
                lKnownIDs.append(record.version)

        lIdsOK = []
        lIdsNotOK = []
        for ID in lIds:
            if ID in lKnownIDs:
                lIdsOK.append(ID)
            else:
                lIdsNotOK.append(ID)

        logging.info("{} IDs to download, {} IDs available".format(len(lIdsNotOK), len(lIdsOK)))
        return lIdsNotOK, lIdsOK

    def run(self):

        # set email
        Entrez.email = self._email
        Entrez.api_key = "e65fd16e08c145d1e72201fd382adabc7708" #api_key nlapalu
        lIds = []
        lIdsOK = []
        init_start = 0
        if self.query:
            # get IDs list
            lIds = self.getIDList()
            # save list
            fh = self.saveIDList(lIds)
            # convert in Python standard datatype
            lIds = self.convertIDList(lIds)
        if self.start and self.IDs and not self.update:
            init_start = self.start
            lIds = self.getIDListFromFile()

        if self.update:
            lIds, lIdsOK = self.getIDListToUpdate(lIds)
            files = [ f for f in glob.glob("tmp/*.gb")]
            for fi in files:
                logging.info("removing {}".format(fi))
                os.remove(fi)

        # get IDs
        files = []
        os.makedirs("tmp", exist_ok = True)
        procs = 1
        if procs > 1:
            pool = multiprocessing.Pool(procs)
            res_file = [pool.apply_async(self.downloadEntries, (start, min(start + self._batch_size, len(lIds)), lIds[start: min(start + self._batch_size,len(lIds))])) for start in range(init_start, len(lIds), self._batch_size)]
            for i,r in enumerate(res_file):
                f = r.get()
                files.append(f)
                pass
            pool.close()
            pool.join()
        else:
            for start in range(0, len(lIds), self._batch_size):
                    end = min(start + self._batch_size, len(lIds))
                    f = self.downloadEntries(start, end, lIds[start:end])
                    files.append(f)

        # if restart dowload, concatenate with previous files
        if self.start and self.IDs and not self.update:
            files = [ f for f in glob.glob("tmp/*.gb")]

        if self.update:

            #out = open(self.output,'wb')
            records = []

            # get old records
            with open(self.update) as handle:
                for record in SeqIO.parse(handle, "genbank"):
                    if record.id in lIdsOK:
                       records.append(record)
            handle.close()
            # get new records
            for f in glob.glob("tmp/*.gb"):
                with open(f) as handle:
                    print(f)
                    for record in SeqIO.parse(handle, "genbank"):
                        if record.id in lIds:
                #           record.name("toto")
                           records.append(record)
                handle.close()
            # out
            SeqIO.write(records, self.output, "genbank")

        else:
            with open(self.output,'wb') as wfd:
                for f in files:
                    with open(f,'rb') as fd:
                        shutil.copyfileobj(fd, wfd)

        if self.status:
            logging.error("Something goes wrong during one download, check consistency, and retry to update with --update")



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("email", help="email", type=str)
    parser.add_argument("output", help="output genbank filename", type=str)
    parser.add_argument("--actin", help='run the predefined actin query, default=False', action="store_true", default=False)
    parser.add_argument("--its", help='run the predefined ITS query, default=False', action="store_true", default=False)
    parser.add_argument("--ef1", help='run the predefined EF1 query, default=False', action="store_true", default=False)
    parser.add_argument("--query", help='run the query', type=str, default="")
    parser.add_argument("--check_query", help='Only count the number of entries returned by the query, no download, default=False', action="store_true", default=False)
    parser.add_argument("--IDs", help='File with IDs', type=str)
    parser.add_argument("--start", help='start accession in ID file', type=int)
    parser.add_argument("--update", help='update a gb file with query', type=str)
    parser.add_argument("-v", "--verbosity", type=int, choices=[1,2,3],
                            help="increase output verbosity 1=error, 2=info, 3=debug")
    args = parser.parse_args()

    logLevel='ERROR'
    if args.verbosity == 1:
        logLevel = 'ERROR'
    if args.verbosity == 2:
        logLevel = 'INFO'
    if args.verbosity == 3:
        logLevel = 'DEBUG'
    logging.getLogger().setLevel(logLevel)

    i = DownloadEntriesFromGenbank(args.email, args.output, args.actin, args.its, args.ef1, args.query, args.check_query, args.IDs, args.start, args.update)
    i.run()




